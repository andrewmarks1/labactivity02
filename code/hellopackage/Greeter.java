package hellopackage;
import secondpackage.Utilities;
import java.util.Scanner;

public class Greeter{
    public static void main(String[] args){
        Utilities utilities = new Utilities();
        Scanner reader = new Scanner(System.in);
        java.util.Random rand = new java.util.Random();
        System.out.println("Enter an integer value");
        int value = reader.nextInt();

        int result = utilities.doubleMe(value);

        System.out.println("The doubled value is: " + result);

    }
}